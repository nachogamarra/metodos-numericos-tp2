#include <iostream>
#include <fstream>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

///Funciones Auxiliares

/// Promedio de los valores de un vector
double promedio(double v[], int n){

    double prom = 0;
    for (int i = 0; i<n; i++){
        prom += fabs(v[i]);
    }
    prom = prom / (n + 0.0);
    return prom;
}

///Calcular el maximo en un vector

double maximo(double v[], int n){

    double aux = fabs(v[0]);

    for (int i = 1; i<n; i++){
        if (fabs(v[i])>aux){
            aux = fabs(v[i]);
        }
    }

    return aux;
}


///Swap
void swapear(double &a, double &b){
    double aux=a;
    a = b;
    b = aux;
}

///Matriz Traspuesta

void trasponerMatriz(double *m, int n){

    for (int i = 0; i<n; i++){
        for (int j = i+1; j<n; j++){
            swapear(m[i*n+j],m[j*n+i]);
        }
    }
}

///Copiar Matriz

void copiarMatriz(double *a, double *b, int n){

    for (int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
            b[i*n+j]=a[i*n+j];
        }
    }
}

///Imprimir matriz
void imprimirMatriz(double *m, int n){
    for (int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
        cout <<  m[i*n+j] << "  ";
        }
        cout << endl;
    }
}

///Imprimir vector
void imprimirVector(double *v,int n){
    cout <<  "[";
    for (int i = 0; i<n; i++){
        cout <<  v[i] << "  ";
    }
    cout << "]" << endl;

}

///Producto de Matrices de nxn

void prod_mat(double *a, double *b, double *axb, int n){
	double res;
	double* aux = new double[n*n];

	for (int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
            res=0;
            for(int k=0; k<n; k++){
                res+=a[i*n+k]*b[k*n+j];
            }
            aux[i*n+j] = res;
        }
    }
    copiarMatriz(aux,axb,n);
    
    delete[] aux;
}

///Swapear Filas

void swapearFilas(double *m,int i ,int k, int n){
    //intercambio la fila "i" con la fila "k"
    double* vec_aux = new double[n];
    for (int j = 0; j<n; j++){
        vec_aux[j]=m[i*n+j];
    }

    for (int j = 0; j<n; j++){
        m[i*n+j]=m[k*n+j];
    }

    for (int j = 0; j<n; j++){
        m[k*n+j]=vec_aux[j];
    }

	delete[] vec_aux;
}

///Pivoteo Parcial
void pivoteoParcial(double *m, double *l, double *p,int k, int n){
    //busco el maximo a partir de la fila i en la columna k
    double max=fabs(m[k*n+k]);
    int pos=k;              //para saber que filas intercambiar
    for (int j=k; j<n; j++){
        if (fabs(m[j*n+k])>max) {
            max=fabs(m[j*n+k]);
            pos=j;
        }
    }

    swapearFilas(m,k,pos,n);
    swapearFilas(p,k,pos,n);
    swapearFilas(l,k,pos,n);

    //rellenar triangular superior de l con 0's

    for (int i = 0; i<n; i++){
        for (int j = i; j<n; j++){
            if (i==j){
                l[i*n+j]=1;
            } else {
                l[i*n+j]=0;
            }
        }
    }
}


///Funciones Principales

///Vector de Frecuencias
void calcularG (double *v,int n){
	for (int i=0; i<n; i++){
		v[i]=i;
	}
}

///Vector de Muestreo
void calcularS (double *v,int n){
	const double pi=3.141592653589793238462643383279;
	for (int i=0; i<n; i++){
		v[i]=(i+0.5)*pi / (n + 0.0);
	}
}

///Constante de normalizacion
double c_k(int k, int n){

	double aux = sqrt(n);

	if (k==1) {
		aux = 1/aux;
    }else{
        aux = (sqrt(2))/aux;
    }

    return aux;
}

///Producto matriz * vector
void prod_int(double *m,double *x, double *b, int n){
	double res;

	for (int i = 0; i<n; i++){
	    res = 0;
        for (int j = 0; j<n; j++){
            res += m[i*n+j] * x[j];
        }
        b[i] = res;
    }
}

///T
void calcularT (double *t, double *g, double *s, int n){
    for (int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
            t[i*n+j]=cos(g[i]*s[j]);
        }
    }
}

void calcularMsombrero(double *m, int n){

		double aux;

		for(int i = 0; i<n; i++){
		    aux = c_k(i+1,n);
			for(int j = 0; j<n; j++){
                m[i*n+j]=aux*m[i*n+j];
			}
		}
}

void calcularM (double *m, int n, double q){
    for (int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
            m[i*n+j] = floor((m[i*n+j]*q + 1)/2.0);
        }
    }
}


/// Metodo 1 de transformacion: Se acomoda el INDICE del 10% de los coeficientes mas prominentes para poder borrar al resto.
void Porcentaje(double v[], int n){

    double auxd, max;
    double* copia = new double[n];
    int aux, indice_max, cota_inf = n/10;
    int* indices = new int[n]; //Uso un arreglo de indices para no tener que modificar el orden del arreglo de datos.

    for (int i = 0; i<n; i++){
        copia[i] = v[i];
        indices[i] = i ;
    }

	// Ordeno de mayor a menor
    for (int i = 0; i <cota_inf; i++){

        indice_max = i;
        max = fabs(copia[indice_max]);

        for (int j = i+1; j<n; j++){
            auxd = fabs(copia[j]);
            if (auxd > max){
                indice_max = j;
                max = auxd;
            }
        }

        auxd = copia[indice_max];
        copia[indice_max] = copia[i];
        copia[i] = auxd;

        aux = indices[indice_max];
        indices[indice_max] = indices[i];
        indices[i] = aux;
    }

    // Elimina las frecuencias mas peque�as (el 90%).
    for(int i = cota_inf; i<n; i++){
        v[indices[i]] = 0;
    }

	delete[] indices;
	delete[] copia;
}



/// Metodo 2 de transformacion: obtengo un BETA y y lo uso como umbral para eliminar ruido de una se�al
void Umbralizar(double senialDCT[], int n){

    double beta = promedio(senialDCT,n);

    for (int i = 0; i<n; i++){
        if (fabs(senialDCT[i]) <= beta){
            senialDCT[i] = 0;
        }
    }

}

/// Calcula la maxima diferencia entre 2 puntos consecutivos de una se�al
double diferencia_maxima(double* senal, int n){

	double aux, max = 0;
	
	for(int i = 1; i < n; i++){
		aux = fabs(senal[i] - senal[i-1]);
		if(aux > max){
			max = aux;
		}
	}
	return max;
}

/// Calcula la diferencia entre el punto mas alto y el punto mas bajo de una se�al
double rango(double* senal, int n){

	double max, min;
	
	max = senal[0];
	min = max;
	
	for(int i = 1; i < n; i++){
		if(senal[i] < min){
			min = senal[i];
		}
		if(senal[i] > max){
			max = senal[i];
		}
	}
	return (max - min);
}

double ecm(double xOriginal[], double xRecuperada[], int n){

    double* acum = new double[n];
	double prom;

    for (int i = 0; i<n; i++){
        acum[i] = xOriginal[i] - xRecuperada[i];
        acum[i] = acum[i]*acum[i];
    }

	prom = promedio(acum,n);

	delete[] acum;
    return prom;
}

double psnr(double xOriginal[], double xRecuperada[], int n){

    double aux = ecm(xOriginal,xRecuperada,n);
    
    double max = rango(xOriginal,n);
    
    max = max*max;

    aux = 1/aux;
    
    aux = aux * max * max;

    aux = log10(aux);

    aux = 10*aux;

    return aux;
}

///Factorizaci�n LU con pivoteo
void factorizacionLUpp(double *m, double *l, double *u, double *p, int n){

    //Inicializo L y P
    for(int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
            if (i==j){
                l[i*n+j]=1;
                p[i*n+j]=1;
            } else {
                l[i*n+j]=0;
                p[i*n+j]=0;
            }
        }
    }

    //Inicializo U
    copiarMatriz(m,u,n);

    double debug;
    //Eliminacion Gaussiana
    for(int k = 0; k<n-1; k++){
        pivoteoParcial(u,l,p,k,n);
       for(int i = k+1; i<n; i++){

            l[i*n+k]=u[i*n+k]/u[k*n+k];
            debug=l[i*n+k];
            for(int j=k+1; j<n; j++){
                u[i*n+j]=u[i*n+j]-(u[k*n+j]*l[i*n+k]);
            }
            u[i*n+k]=0;
        }
    }

}

///Backward Substitution
void backwardSubstitution(double *m, double *x, double *b, int n){
    double sumatoria;
    for(int i = n-1; i>=0; i--){
        sumatoria=0;
        for (int j = i+1; j<n; j++){
            sumatoria+=m[i*n+j]*x[j];
        }
        x[i]=(b[i]-sumatoria)/m[i*n+i];
    }
}

///Forward Substitution
void forwardSubstitution(double *m, double *x, double *b, int n){
    for(int i = 0; i<n; i++){
        x[i]=b[i];
        for (int j = 0; j<i; j++){
            x[i]-=m[i*n+j]*x[j];
        }
    }
}

void cargar_imagen(double* &matriz_imagen, int& n, char* nombre){

	unsigned char pixel;
	char aux[4];
	ifstream archivo;
	archivo.open(nombre);

	while (!archivo.good()){
          cout << "El archivo no existe. Ingrese otro: ";
          cin >> nombre;
          cout << endl;
          archivo.open(nombre);
    }

	archivo >> aux;
	archivo >> n;
	archivo >> aux;			// Asumo imagenes cuadradas
	archivo >> aux;
	
	matriz_imagen = new double[n*n];

	for(int i = 0; i < n*n; i++){
		pixel = archivo.get();
		matriz_imagen[i] = pixel;
	}

	archivo.close();
}

void cargar_senal(double* &senal, int &n, char* nombre){

	ifstream archivo;
	archivo.open(nombre);
	
	while(!archivo.good()){
          cout << "El archivo no existe. Ingrese otro: ";
          cin >> nombre;
          cout << endl;
          archivo.open(nombre);
    }
	
	archivo >> n;
	
	senal = new double[n];
	
	for(int i = 0; i < n; i++){
	
		archivo >> senal[i];
	
	}

}

void guardar_imagen(double* a, const char* nombre, int n){

	unsigned char pixel;
	ofstream salida;
	salida.open(nombre);
	int i;
	salida << "P5" << endl;
	salida << n << " " << n << endl;
	salida << "255" << endl;

	for(i = 0; i < n*n; i++){
		pixel = a[i];
		salida << pixel;
	}
	
	salida.close();
}

void guardar_matriz(double* a, const char* nombre, int n, int m){

	unsigned char aux;
	ofstream salida;
	salida.open(nombre);

	salida << n << endl;	//filas
	salida << m << endl;	//columnas

	//matriz
	for (int i = 0; i < n; i++){
		for (int j = 0; j < m-1; j++){
			salida << a[m*i+j] << "\t";
		}
		salida << a[m*i+m-1] << endl;
	}

	salida.close();
}

/// Formato Matlab
void guardar_senal(double* a, const char* nombre, int n){

	ofstream salida;
	salida.open(nombre);

	salida << n << endl;

	for(int i = 0; i<n; i++){
        salida << a[i] << " ";
    }

	salida.close();
}

/// Formato Gnuplot
void guardar_senales(double* a, double* b, double* c, double* d, double* e, const char* nombre, int n){

	ofstream salida;
	salida.open(nombre);

	for(int i = 0; i<n; i++){
        salida << i << "\t" << a[i] << "\t" << b[i] << "\t" << c[i] << "\t" << d[i] << "\t" << e[i] << endl;
    }

	salida.close();
}

/// Agrega ruido en rango [-diferencia_maxima/2, diferencia_maxima/2]
void agregarRuido(double* senal, int n){

	int aux;
	double max = diferencia_maxima(senal,n);

	max = max;						/// considero un porcentaje del rango maximo
	
	for(int i = 0; i < n; i++){
		senal[i] += ((rand() % 100) * max)/100 - max/2;
	} 

}

double mover_senal(double *v, int n){

    int i;

    double min=0;

    for (i = 0; i<n; i++){
        if (v[i] < min) min = v[i];
    }

    for (i = 0; i<n; i++) {
        v[i] -= min;
    }

    return min;

}

void reajustar_senal(double* v, const double valor, int n){
	
	for(int i = 0; i < n; i++){
		v[i]+=valor;
	}
	
}

void redondearVector(double* v, int n){

    for (int i=0; i<n; i++){
        v[i]=round(v[i]);
    }

}

int main(){

    int n, opcion, opcion_metodo;
    double q;
    char nombre[20];
    double* x;
    double* y;
	double* y_con_ruido;
	double* x_sin_ruido;
	double* x_recuperada;
	double* v;

	/// Elijo trabajar con se�ales o imagenes
    //cout << "1) Trabajar con imagen. " << endl;
    //cout << "2) Trabajar con se�al. " << endl;
    //cin >> opcion;
    //cout << endl;
    opcion = 2;
    //cout << "Archivo a Abrir: ";
    //cin >> nombre;
    //cout << endl;
    sprintf(nombre,"signals/g450.txt");
    
    if(opcion==1){
        /// Cargo mi imagen sin ruido
        cargar_imagen(x,n,nombre);

		q = maximo(x,n*n);
    } else
    if(opcion==2){
        /// Cargo mi se�al sin ruido
        cargar_senal(x,n,nombre);
        
        y = new double[n];
		y_con_ruido = new double[n];
		x_sin_ruido = new double[n];
		x_recuperada = new double[n];
		v = new double[n];
		
		q = maximo(x,n);
        
    } else {
        cout << "Seleccion no valida." << endl;
        return 0;
    }

    double* g = new double[n];
    double* s = new double[n];
    double* m = new double[n*n];
	double* l = new double[n*n];
    double* u = new double[n*n];
    double* p = new double[n*n];

    calcularG(g,n);
    calcularS(s,n);
    calcularT(m,g,s,n);
    calcularMsombrero(m,n);
    //calcularM(m,n,q);
    
	/// Hago la factorizacion LU de M
    //factorizacionLUpp(m,l,u,p,n);

	if (opcion==1){
        /// imagen
        /// Le agrego ruido a X
		agregarRuido(x,n*n);
		
		guardar_imagen(x,"imagen_ruidosa.pgm",n);
		
		///Obtengo Y, la DCT de X, y la guardo en x
		prod_mat(m,x,x,n);
		trasponerMatriz(m,n);
		prod_mat(x,m,x,n);
		
		guardar_imagen(x,"transformada_dct.pgm",n);
    }
    
    if (opcion==2){
        /// se�al
        /// Hago copia para informe
        for(int i = 0; i<n; i++){
			x_sin_ruido[i] = x[i];
		}
		
        /// Le agrego ruido a x
		agregarRuido(x,n);
		
		mover_senal(x,n);
		redondearVector(x,n);
		//reajustar_senal(x,-5,n);
		q = maximo(x,n);
		calcularM(m,n,10000*q);
		factorizacionLUpp(m,l,u,p,n);
		
		/*guardar_matriz(l,"l.txt",n,n);
		guardar_matriz(u,"u.txt",n,n);
		guardar_matriz(p,"p.txt",n,n);
		guardar_imagen(l,"l.pgm",n);
		guardar_imagen(u,"u.pgm",n);
		guardar_imagen(p,"p.pgm",n);*/
		
		/// Obtengo "y", la DCT de x.
		prod_int(m,x,y,n);
		
		/// Guardo copia de "y sin ruido" para informe
		for(int i = 0; i<n; i++){
			y_con_ruido[i] = y[i];
		}
    }

    //cout << "Seleccionar Metodo de transformacion: "  << endl;
    //cout << "1) Porcentaje. " << endl;
    //cout << "2) Umbralizar. " << endl;
    //cin >> opcion_metodo;
    //cout << endl;

	if (opcion == 1){
		
		/// Elimino ruido de Y usando alguno de estos filtros
		if (opcion_metodo==1){
			/// Metodo 1
			cout << "Metodo elegido: Porcentaje. " << endl;
			Porcentaje(x,n*n);
			guardar_imagen(x,"despues_de_aplicar_porcentaje.pgm",n);
		}
    
		if (opcion_metodo==2){
			/// Metodo 2
			cout << "Metodo elegido: Umbralizar. " << endl;
			Umbralizar(x,n*n);
			guardar_imagen(x,"despues_de_aplicar_umbralizar.pgm",n);
		}


		/// Ajusto Y' a la matriz de permutacion
		prod_mat(p,x,x,n);
		trasponerMatriz(p,n);
		prod_mat(x,p,x,n);
		trasponerMatriz(x,n);
		
		/// Recupero un X' sin ruido
		for(int i = 0; i < n; i++){
			forwardSubstitution(l,&(x[i*n]),&(x[i*n]),n);
		}
		for(int i = 0; i < n; i++){
			backwardSubstitution(u,&(x[i*n]),&(x[i*n]),n);
		}
		trasponerMatriz(x,n);
		for(int i = 0; i < n; i++){
			forwardSubstitution(l,&(x[i*n]),&(x[i*n]),n);
		}
		for(int i = 0; i < n; i++){
			backwardSubstitution(u,&(x[i*n]),&(x[i*n]),n);
		}
		
		guardar_imagen(x,"salida.pgm",n);
	}

	if (opcion == 2){
		//if (opcion_metodo==1){
			/// Metodo 1
			cout << "Metodo elegido: Porcentaje. " << endl;
			Porcentaje(y,n);
		//}
    
		//if (opcion_metodo==2){
			/// Metodo 2
			//cout << "Metodo elegido: Umbralizar. " << endl;
			//Umbralizar(y,n);
		//}
		
		///Reconstruccion de la se�al
		prod_int(p,y,v,n);
		forwardSubstitution(l,v,v,n);
		backwardSubstitution(u,x_recuperada,v,n);
		
		///salida Matlab
		guardar_senal(x_sin_ruido,"salida_matlab_x_sin_ruido.txt",n);
		guardar_senal(x,"salida_matlab_x_con_ruido.txt",n);
		guardar_senal(x_recuperada,"salida_matlab_x_recuperada.txt",n);

		///salida Gnuplot
		guardar_senales(x_sin_ruido,x,x_recuperada,y_con_ruido,y,"salida_gnuplot.txt",n);
		
		delete[] y;
		delete[] v;
		delete[] x_sin_ruido;
		delete[] x_recuperada;
		delete[] y_con_ruido;
	}

	cout << "Resultados guardados en los archivos:" << endl << "salida_gnuplot.txt, salida_matlab_x_sin_ruido.txt si es se�al, salida_matlab_x_con_ruido.txt, salida_matlab_x_recuperada.txt." << endl << "salida.pgm si es imagen." << endl;

	delete[] x;
    delete[] g;
    delete[] s;
    delete[] m;
	delete[] l;
    delete[] u;
    delete[] p;
    

	return 0;
}
